import Navbar from './components/Navbar/Navbar';
import Center from './components/Center/Center';

function App() {
  return (
    <div className="App">
        <Navbar> </Navbar>
        <Center/>
    </div>
  );
}

export default App;
