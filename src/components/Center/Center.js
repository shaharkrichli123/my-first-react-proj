import React from "react";
import WeatherCard from "../WeatherCard/WeatherCard";
import useStyles from "./CenterStyle";
import Grid from "@material-ui/core/Grid";
import weatherCitiesReq from "@jsons/WeatherCities";

const Center = () => {
  const classes = useStyles();

  return (
    <div className={classes.weatherCenterContainer}>
      <Grid container classes={{ container: classes.weatherCardsContainer }}>
        {weatherCitiesReq.cities.map((city, index) => (
          <Grid key={index} item xs={5}>
            <WeatherCard cityName={city} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default Center;
