import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    weatherCardsContainer: {
        justifyContent: 'end',
        display: 'flex'
    },
    weatherCenterContainer: {
        backgroundColor: '#40E0D0',
        height: 'calc(100vh - 3rem)'
    }
  }));

  export default useStyles;