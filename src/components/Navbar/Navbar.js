import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import useStyles from "./NavbarStyle";

const Navbar = () => {

  const classes = useStyles();

  return (
    <div>
      <AppBar position="static" className={classes.weatherNavbar}>
        <Typography edge="start" variant="h6">
          תחזית מסביב לעולם
        </Typography>
      </AppBar>
    </div>
  );
};

export default Navbar;
