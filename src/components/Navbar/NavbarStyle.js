import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    weatherNavbar: {
        height: '3rem',
        justifyContent: 'center',
        paddingRight: '2rem'
    }
  }));

  export default useStyles;