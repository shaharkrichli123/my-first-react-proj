import React, { useEffect, useState } from "react";
import Card from "@material-ui/core/Card";
import useStyles from "./WeatherCardStyle.js";
import WeatherCardHeader from "./WeatherCardHeader.js";
import WeatherCardInfoDisplay from "./WeatherCardInfoDisplay.js";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import weatherInfoReq from "@jsons/WeatherInfoReq";
import axios from "axios";

const WeatherCard = (props) => {
  const classes = useStyles();
  const [cityName, setCityName] = useState("");
  const [cityWeatherDescription, setCityWeatherDescription] = useState("");
  const [weatherInfoReqTitles] = useState(weatherInfoReq);
  const [weatherMainData,setWeatherMainData] = useState({})

  useEffect(() => {
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${props.cityName}&lang=he&units=metric&appid=7ae8ccfbb3c794652c1916af0004862d`
      )
      .then((results) => {
        setCityName(results.data.name);
        setCityWeatherDescription(results.data.weather[0].description);
        setWeatherMainData(results.data.main)
        console.log(results.data);
      });
  }, [props.cityName]);

  return (
    <div>
      <Card className={classes.weatherCard}>
        <WeatherCardHeader
          cityName={cityName}
          cityWeatherDesc={cityWeatherDescription}
        />
        <Container maxWidth="xs">
          <Grid
            container
            className={classes.weatherCardInfoDisplayerGrid}
            spacing={1}
          >
            {weatherInfoReqTitles.infoTitles.map((currReq, index) => (
              <Grid item xs={4}>
                <WeatherCardInfoDisplay currReq={currReq} weatherMainData={weatherMainData} />
              </Grid>
            ))}
          </Grid>
        </Container>
      </Card>
    </div>
  );
};

export default WeatherCard;
