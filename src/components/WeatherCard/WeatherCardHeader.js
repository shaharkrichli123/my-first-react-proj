import React from "react";
import Grid from "@material-ui/core/Grid";
import useStyles from "./WeatherCardHeaderStyle.js";
import WbSunnyIcon from "@mui/icons-material/WbSunny";
import Typography from "@material-ui/core/Typography";

const WeatherCardHeader = (props) => {
  const classes = useStyles();

  return (
    <div>
      <Grid container className={classes.WeatherCardHeaderGridContainer}>
        <Grid item xs={9}>
          <Typography
            align="right"
            variant="h5"
            classes={{ h5: classes.weatherCardMainTitle }}
          >
            {props.cityName}
          </Typography>
        </Grid>
        <Grid item xs={3} className={classes.WeatherCardHeaderIcon}>
          <Typography>
            <WbSunnyIcon fontSize="large" />
          </Typography>
        </Grid>
        <Grid item xs="auto">
          <Typography
            align="right"
            variant="body2"
            classes={{ body2: classes.weatherCardSecondaryTitle }}
          >
            {props.cityWeatherDesc}
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
};

export default WeatherCardHeader;
