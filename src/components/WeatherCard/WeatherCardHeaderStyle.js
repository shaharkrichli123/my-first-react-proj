import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  WeatherCardHeaderGridContainer: {
      marginTop: '1.5rem',
      marginRight: '1.5rem'
    },
    weatherCardMainTitle: {
        fontWeight: 'bolder !important',
    },
    weatherCardSecondaryTitle: {
        fontSize: '0.70rem !important',
        marginTop: '-0.5rem !important',
        color:"lightslategray",
    },
    WeatherCardHeaderIcon: {
      display: 'flex',
      color: 'red',
    }
  }));

  export default useStyles;