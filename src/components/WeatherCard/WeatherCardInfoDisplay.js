import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import useStyles from "./WeatherCardInfoDisplayStyle";

const WeatherCardInfoDisplay = (props) => {

  const classes = useStyles();

  return (
    <div>
        <Grid container direction="column">
          <Grid item xs={12}>
            <Typography variant="body2" align="center" classes={{body2: classes.weatherCardInfoTitle}}>
              {props.currReq.title}
            </Typography>
            <Typography variant="h5" align="center">
              {props.weatherMainData[props.currReq.weatherObjectDesc]}
            </Typography>
          </Grid>
        </Grid>
    </div>
  );
};

export default WeatherCardInfoDisplay;
