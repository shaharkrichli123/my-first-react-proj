import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    weatherCardInfoTitle: {
        marginBottom: '1rem'
    }
  }));

  export default useStyles;