import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    weatherCard: {
      backgroundColor: 'white',
      width: '19rem',
      height: '12rem',
      margin: '2rem 0'
    },
    weatherCardInfoDisplayerGrid: {
      marginTop: '1rem !important',
    }
  }));

  export default useStyles;